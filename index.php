<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="dist/lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="dist/css/style.css">
</head>
<body>

<?php
    include('list.php');
?>

    <section>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Brand</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">Link</a></li>
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                    </li>
                </ul>
                <form class="navbar-form navbar-left">
                    <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#">Link</a></li>
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                    </ul>
                    </li>
                </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </section>
    

    <section style="margin-bottom:80px;">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                <form action="store.php" method="POST">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input name="name" type="text" class="form-control" id="" placeholder="Full Name">
                    </div>
                    <div class="form-group">
                        <label for="">Roll</label>
                        <input name="roll" type="text" class="form-control" id="" placeholder="Roll">
                    </div>

                    <div class="form-group">
                        <label for="">Bangla</label>
                        <input name="bangla" type="text" class="form-control" id="" placeholder="Bangla Number">
                    </div>
                    <div class="form-group">
                        <label for="">English</label>
                        <input name="english" type="text" class="form-control" id="" placeholder="English Number">
                    </div>
                    <div class="form-group">
                        <label for="">Math</label>
                        <input name="math" type="text" class="form-control" id="" placeholder="Math Number">
                    </div>

                    
                    
                    <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Roll</th>
                                <th>Bangla</th>
                                <th>English</th>
                                <th>Math</th>
                                <th>Result</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($data as $item){?>
                            <tr>
                                <td><?php echo $item['name']; ?></td>
                                <td><?php echo $item['roll']; ?></td>
                                <td><?php echo $item['bangla']." "; cgpa($item['bangla']); ?></td>
                                <td><?php echo $item['english']." "; cgpa($item['english']) ?></td>
                                <td><?php echo $item['math']." "; cgpa($item['math']) ?></td>
                                <td>
                                <?php echo $item['bangla']+$item['english']+$item['math']; ?>
                                </td>  
                            </tr>
                            <?php } ?>

                            <?php 
                                function cgpa($num){
                                    if($num<30){
                                        echo "Fail";
                                    }elseif($num<60){
                                        echo "D";
                                    }elseif($num<80){
                                        echo "C";
                                    }elseif($num<100){
                                        echo "B";
                                    }
                                }
                            
                            ?>
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </section>


    <script src="dist/lib/jquery/jquery.js"></script>
    <script src="dist/lib/bootstrap/js/bootstrap.js"></script>
</body>
</html>